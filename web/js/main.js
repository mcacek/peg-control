require.config({
  //baseUrl: "/another/path",
    paths : {
        "jquery" : "components/jquery/jquery",
        "handlebars" : "components/handlebars/handlebar",
        "underscore" : "components/underscore/underscore"
    },
    waitSeconds: 15
});

requirejs( [ 'app/board', 'app/dude' ], function( board, dude ) {
    var dude1 = dude.create( 'Cosmo' );
    board.addDude( dude1 );
    dude1.pegUp(8);
    dude1.pegDown(3);
})