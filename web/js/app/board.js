define( [ 'jquery', './dude' ], function() {
    return function() {
        var board
            , dudes = {};

        function initialize() {
            console.log( 'Initializing board.' );
        }

        board = {
            addDude : function( dude ) {
                console.log( 'Adding dude.' );
            },

            removeDude : function( dude ) {
                console.log( 'Removing dude.' );
            }
        };

        initialize();

        return board;
    }();
});