define( [], function() {
	return function() {
		var dude = {
			create : function( name, _pegCount ) {
				_pegCount = _pegCount || 0;

				console.log( 'creating dude named ' +name );
				return {
					pegUp : function( pegCount ) {
						pegCount = pegCount || 1;
						_pegCount += pegCount;

						if ( pegCount ) {
							console.log( 'Moving ' +name +' up ' +pegCount +' peg(s) to ' +_pegCount +'!' );
						}
						else {
							console.log( 'Moving up a peg!' );
						}
					},
					pegDown : function( pegCount ) {
						pegCount = pegCount || 1;
						_pegCount -= pegCount;
						console.log( 'Moving ' +name +' down ' +pegCount +' peg(s) to ' +_pegCount +'!' );
					},
					setPegCount : function( pegCount ) {
						_pegCount = pegCount;
					},
					getPegCount : function() {
						return _pegCount;
					}
				};
			}
		};

		return dude;
	}();
});
