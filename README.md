Peg Control!

Peg Control is the digital manifestation of the "you just went up a peg in my book!" conversational tactic. It's commonly stated but rarely tracked.  I think that's a shame so I created a simple tool that lets you track your pegs!